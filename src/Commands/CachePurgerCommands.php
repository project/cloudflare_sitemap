<?php

namespace Drupal\cloudflare_sitemap\Commands;

use Consolidation\SiteAlias\SiteAliasManagerAwareTrait;
use Drupal\cloudflare_sitemap\CachePurger;
use Drush\Commands\DrushCommands;
use Drush\SiteAlias\SiteAliasManagerAwareInterface;
use Drupal\Core\Url;

/**
 * CacheCommands class.
 */
class CachePurgerCommands extends DrushCommands implements SiteAliasManagerAwareInterface {

  use SiteAliasManagerAwareTrait;

  /**
   * Cloudflare cache purger service.
   *
   * @var \Drupal\cloudflare_sitemap\CachePurger
   */
  protected $cachePurger;

  /**
   * CacheCommands constructor.
   *
   * @param \Drupal\cloudflare_sitemap\CachePurger $cache_purger
   *   Cloudflare cache purger service.
   */
  public function __construct(CachePurger $cache_purger) {
    parent::__construct();
    $this->cachePurger = $cache_purger;
  }

  /**
   * Purge sitemap URLs from Cloudflare CDN cache.
   *
   * @param string $url
   *   The URL to the sitemap.
   *
   * @return bool
   *   TRUE on success.
   *
   * @command cloudflare-purge-sitemap
   * @aliases cfps
   */
  public function purgeSitemap(string $url = '') {
    // Try to get url from environmental variables if not provided.
    $host = getenv('BACKEND_HOST');
    if (empty($url) && !empty($host)) {
      $url = "https://$host/sitemap.xml";
    }
    // Resort to relying on URL object.
    if (empty($url)) {
      $url = Url::fromRoute('<>', [], ['absolute' => TRUE])->toString();
    }

    $this->io()->writeln(dt("Purging @url", ['@url' => $url]));

    // Inject HTTP AUTH if found in environmental variables.
    if (getenv('HTTP_AUTH_USER') && getenv('HTTP_AUTH_PASS')) {
      $user = getenv('HTTP_AUTH_USER');
      $pass = getenv('HTTP_AUTH_PASS');
      $url = str_replace('https://', "https://$user:$pass@", $url);
    }

    // Flush CDN cache for the website via sitemap.
    return $this->cachePurger->purgeSitemap($url, $this->io(), 'dt');
  }

}
