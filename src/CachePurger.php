<?php

namespace Drupal\cloudflare_sitemap;

use Cloudflare\API\Adapter\Guzzle;
use Cloudflare\API\Auth\APIToken;
use Cloudflare\API\Endpoints\Zones;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\State\StateInterface;
use GuzzleHttp\ClientInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class implements service cloudflare_sitemap.cache_purger.
 */
class CachePurger {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The config for Cloudflare API.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a CachePurger object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   Logger channel.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   */
  public function __construct(ClientInterface $http_client, ConfigFactoryInterface $config_factory, StateInterface $state) {
    $this->httpClient = $http_client;
    $this->config = $config_factory->get('cloudflare.settings');
    $this->state = $state;
  }

  /**
   * Ask Cloudflare API to purge cache for specified URLs.
   *
   * @param array $urls
   *   Array of URLs from any supported domains.
   * @param \Symfony\Component\Console\Style\SymfonyStyle $io
   *   Input / output controller of drush.
   * @param callable $t
   *   Callable of a string formatter.
   *
   * @return bool
   *   TRUE if Cloudflare accepted the request.
   */
  public function purgeFromCloudflare(array $urls, SymfonyStyle $io, callable $t): bool {
    // No need to purge cache if website in the maintenance mode.
    if ($this->state->get('system.maintenance_mode')) {
      $io->info($t('Cache purge is skipped, because maintenance mode is enabled.'));
      return TRUE;
    }

    $api_token = $this->config->get('api_token');
    $zone_id = $this->config->get('zone_id');

    // We save all URLs purged in one request to avoid unnecessary duplicated
    // API calls to Cloudflare.
    $already_flushed_urls = &drupal_static('cloudflare_already_purged_urls', []);

    if (empty($urls) || empty($api_token) || empty($zone_id)) {
      // It's fine to return TRUE if it's DRY RUN mode (dev environments).
      return TRUE;
    }

    $not_purged_urls = array_diff($urls, $already_flushed_urls);
    if (!$not_purged_urls) {
      return TRUE;
    }

    $purge_urls = array_chunk($urls, 30);

    try {
      // Init Cloudflare API.
      $key = new APIToken($api_token);
      $adapter = new Guzzle($key);
      $zonesApi = new Zones($adapter);

      $status = FALSE;

      foreach ($purge_urls as $i => $urls_chunk) {
        $status = $zonesApi->cachePurge($zone_id[0], $urls_chunk);

        if ($status) {
          // Add flushed URLs in static variable. Note that we use plain strings
          // to simplify comparing of arrays in $this->purgeMultiple.
          $already_flushed_urls = array_merge($already_flushed_urls, $urls_chunk);

          $io->writeln($t('Purged URLs from Cloudflare (part @part out of @parts).', [
            '@part' => $i + 1,
            '@parts' => count($purge_urls),
          ]));
        }
        else {
          // PHP SDK may just return FALSE without specifying an error.
          $io->error($t('Error purging URLs from Cloudflare.'));

          // Do not continue if an error occurred.
          break;
        }
      }

      if ($status) {
        $io->success($t('Successfully purged all @urls urls', ['@urls' => count($urls)]));
      }

      return $status;
    }
    catch (\Exception $exception) {
      $io->error($t('Error purging URLs from Cloudflare. Error %error.', [
        '%error' => $exception->getMessage(),
      ]));
      return FALSE;
    }
    catch (\Throwable $exception) {
      $io->error($t('Error purging URLs from Cloudflare. Error %error.', [
        '%error' => $exception->getMessage(),
      ]));
      return FALSE;
    }

  }

  /**
   * Purges URLs from the external sitemap URL.
   *
   * @param string $sitemap_url
   *   Sitemap URL.
   * @param \Symfony\Component\Console\Style\SymfonyStyle $io
   *   Input / output controller of drush.
   * @param callable $t
   *   Callable of a string formatter.
   *
   * @return bool
   *   TRUE if Cloudflare cache was purged. Otherwise, FALSE.
   */
  public function purgeSitemap(string $sitemap_url, SymfonyStyle $io, callable $t): bool {
    // No need to purge cache if website in the maintenance mode.
    if ($this->state->get('system.maintenance_mode')) {
      $io->info($t('Sitemap cache purge is skipped, because maintenance mode is enabled.'));
      return TRUE;
    }

    try {
      $urls = [];
      $file = file_get_contents($sitemap_url);
      $sitemap = new \SimpleXMLElement($file);
      foreach ($sitemap->url as $url_list) {
        $urls[] = (string) $url_list->loc;
      }

      return $this->purgeFromCloudflare($urls, $io, $t);
    }
    catch (\Exception $exception) {
      $io->error($t('Error purging sitemap %sitemap from Cloudflare. Error %error.', [
        '%sitemap' => $sitemap_url,
        '%error' => $exception->getMessage(),
      ]));
      return FALSE;
    }
  }

}
